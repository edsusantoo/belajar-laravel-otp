<?php

use Illuminate\Http\Request;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



Route::group(['prefix' => 'tugasakhir', 'middleware' => ['api','cors']], function(){

  Route::post('/login', 'Api\UsersController@LoginUser');
  Route::post('/register','Api\UsersController@RegisterUser');

  Route::post('/getuser','Api\UsersController@GetUserVerify');



  Route::post('/getuserr','Api\UsersController@GetMe');

  Route::post('/date', 'Api\OTPController@GetDate');

  Route::post('/setredis','Api\OTPController@SetRedis');

  Route::post('/getredis','Api\OTPController@GetRedis');


  //ini alur sebenarnya
  Route::post('/reqotp','Api\OTPController@ReqOtp');





});
