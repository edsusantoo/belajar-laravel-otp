<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Redis;


Use App\Models\Users;

//carboon
use Carbon\Carbon;

use Cookie;






//Encryption
use App\Helpers\Crypto\AES;
use App\Helpers\Crypto\RC4;


class OTPController extends Controller
{

  public function __construct(Users $user)
   {
      $this->middleware('auth:api');

   }



  public function GetDate(){
    $data = array();
    // get the current time
    $current = Carbon::now('Asia/Jakarta');

    $waktu_otp = $current->copy()->addMinutes(3);


    $data['status'] = 'Berhasil';
    $data['waktu sekarang'] = $current;
    $data['waktu otp'] = $waktu_otp;


    return Response::json($data);

  }

  public function SetRedis(Request $request){
    $username = $request->get('username');
    $kodeotp = $request->get('kodeotp');
    $waktu = $request->get('waktu');

    $data = array();

    Redis::set('username',$username);
    $test=Redis::set('kodeotp',$kodeotp);
    Redis::set('waktu', $waktu);

    $data['rediskodeotp'] = $username;

    return Response::json($data);

  }

  public function GetRedis(){

    $data = array();

    $test=Redis::get('kodeotp');
    $username  = Redis::get('username');
    $waktu = Redis::get('waktu');

    $data['rediskodeotp'] = $test;
    $data['username'] = $username;
    $data['waktu'] = $waktu;

    return Response::json($data);

  }


  //ini alur sebenarnya
  public function ReqOTP(Request $request){
    //key
    $key = $request->get('key');
    $key = substr(md5($key), 0,16);

    //secretkey
    $secretkey = $request->get('secretkey'); //nopeserta+nolomba+nokelas//+waktu(dd:mm:yyyy+hh:mm:ss)->ini didapatkan dari server
    //get waktu
    $current = Carbon::now('Asia/Jakarta');
    $waktu_otp = substr($current->copy()->addMinutes(3)->toDateTimeString(),11);
    //get secretkey+waktu
    $secretkey = $secretkey.$waktu_otp;


    ### ENCRYPT PROCESS ###
  	$loop 		= (strlen($secretkey) % 16 == 0) ? strlen($secretkey)/16 : intVal(strlen($secretkey)/16) + 1;
  	$cipherText	= "";

    echo $secretkey;

  	for ($i=0; $i<$loop; $i++) {
  		$start    = $i * 16;
  		$txt	  = substr($secretkey, $start, 16);
  		$aes 	 = new AES($key);
  		$enkrip  = $aes->encrypt($txt);
  		$rc4 	 = new RC4($key);
  		$enkrip2 = $rc4->encrypt($enkrip);
  		$cipherText	= $enkrip2;

  	}

    $data = array();

    $data['test'] = $aes;

    return Response::json($data);



  }


}
