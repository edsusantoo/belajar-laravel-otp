<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\View\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;


Use App\Models\Users;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;










class UsersController extends Controller
{

  public function __construct(Users $user)
   {
      $this->middleware('auth:api', ['except' => ['LoginUser']]);

   }
   //
   // public function LoginUser(Request $request)
   //  {
   //    // $credentials = $request->only('username', 'password');
   //    //
   //    //   if ($token = JWTAuth::attempt($credentials)) {
   //    //       return $this->respondWithToken($token);
   //    //   }
   //    //
   //    //   return response()->json(['error' => 'Unauthorized'], 401);
   //
   //
   //          $data = array();
   //
   //
   //
   //          $username = $request->get('username');
   //          $password = $request->get('password');
   //          $credentials = [
   //            'username' => $username,
   //            'password' => $password
   //          ];
   //          $login = Users::where('username', $username)->first();
   //          $token = auth()->attempt($credentials);
   //          if($login){
   //            if( password_verify($password, $login->password)){
   //
   //                $data['status'] = 'Berhasil';
   //                $data['pesan'] = 'Selamat Datang, '. $login->username;
   //                $data['token'] = $token;
   //            }else{
   //                $data['status'] = 'Gagal';
   //                $data['pesan'] = 'Password Anda Salah';
   //                $data['pesann'] = $passwrod;
   //            }
   //          }else{
   //              $data['status'] = 'Gagal';
   //              $data['pesan'] = 'Email anda tidak terdaftar';
   //          }
   //          return Response::json($data);
   //
   //  }
   //
   //
   //    public function RegisterUser(Request $request){
   //        $data = array();
   //        $username = $request->get('username');
   //        $password = bcrypt($request->get('password'));
   //        $alamat = $request->get('alamat');
   //        $notelp = $request->get('notelp');
   //
   //        $cekuser = Users::where('username', $username)->first();
   //
   //        if($cekuser){
   //          $data['status'] = 'Gagal';
   //          $data['pesan'] = 'Username Sudah Terdaftar, Silahkan masukan username yang lain';
   //
   //        }else{
   //          $user = new Users();
   //          $user->username = $username;
   //          $user->password = $password;
   //          $user->alamat = $alamat;
   //          $user->notelp = $notelp;
   //
   //
   //          $data = array();
   //
   //          if($user->save()){
   //            $data['status'] = "Berhasil";
   //            $data['pesan'] = 'Terima kasih telah mendaftar';
   //            $data['username'] = $username;
   //
   //          }
   //
   //        }
   //
   //        return Response::json($data);
   //
   //    }
   //
   //    public function GetUser(Request $request){
   //
   //        $data = array();
   //        $username = $request->get('username');
   //        $login = Users::where('username', $username)->first();
   //
   //        if($login){
   //          $data['status'] = "Berhasil";
   //          $data['username'] = $login->username;
   //          $data['alamat'] = $login->alamat;
   //          $data['notelp'] = $login->notelp;
   //        }else{
   //          $data['status'] = "Gagal";
   //          $data['pesan']  = "User tidak ditemukan";
   //        }
   //
   //
   //        return Response::json($data);
   //
   //    }
   //
   //    public function GetUserVerify(Request $request){
   //
   //      $username = $request->get('username');
   //
   //      $cekuser = Users::where('username', $username)->first();
   //
   //      $response = [
   //        'status' => 'Berhasil',
   //        'data' => $cekuser
   //      ];
   //
   //      return response()->json($response,200);
   //
   //    }
   //
   //
   //


  /**
   * Get a JWT via given credentials.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function LoginUser()
  {
      $credentials = request(['username', 'password']);

      if (! $token = auth()->attempt($credentials)) {
          return response()->json(['error' => 'Unauthorized'], 401);
      }

      return $this->respondWithToken($token);
  }

  /**
   * Get the authenticated User.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function GetMe()
  {
      return response()->json(auth()->user());
  }

  /**
   * Log the user out (Invalidate the token).
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function logout()
  {
      auth()->logout();

      return response()->json(['message' => 'Successfully logged out']);
  }

  /**
   * Refresh a token.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function refresh()
  {
      return $this->respondWithToken(auth()->refresh());
  }

  /**
   * Get the token array structure.
   *
   * @param  string $token
   *
   * @return \Illuminate\Http\JsonResponse
   */
  protected function respondWithToken($token)
  {
      return response()->json([
          'access_token' => $token,
          'token_type' => 'bearer',
          'expires_in' => auth()->factory()->getTTL() * 60
      ]);
  }

}
